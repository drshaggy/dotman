//
// Created by connor on 10/11/2019.
//

#include "headers/Command.h"


using namespace std;


Command* Command::Create(Commands command)
{
    if (command == Commands::help)
        return new Help();
    else if (command == Commands::edit)
        return new Edit();
    else if (command == Commands::ls)
        return new Ls();
    else if (command == Commands::config)
        return new Config();
    else
        return new Help();
}

bool Command::executeCommand(vector<string> args, vector<DotFile>* dot_files)
{
    return false;
}

bool Help::executeCommand(vector<string> args, vector<DotFile>* dot_files)
{
    std::string line;
    ifstream help_file(HELP_FILE_PATH);
    if (help_file.is_open())
    {
        while (getline(help_file, line)) {
            cout << line << endl;
        }
        help_file.close();
        return true;

    }
    else
        return false;
}

bool Edit::executeCommand(vector<string> args, vector<DotFile>* dot_files)
{
    std::string file_to_edit = args[1];
    bool isMatch = false;
    for (auto &dot_file: *dot_files)
    {

        if (dot_file.getName() == file_to_edit)
        {
            std::string c = "$EDITOR ";
            std::string c2 = dot_file.getFilePath();
            std::string c3 = c + c2;
            system(c3.c_str());
            isMatch = true;
        }


    }
    if (not isMatch){
        cout << "Invalid Dot File" << endl;
    }
    return false;
}

bool Ls::executeCommand(vector<string> args, vector<DotFile>* dot_files)
{
    bool isSuccess = false;
    for (auto &dot_file: *dot_files)
    {
        cout << dot_file.getName() << endl;
        isSuccess = true;
    }
    return isSuccess;
}

bool Config::executeCommand(vector<string> args, vector<DotFile> *dot_files)
{
    std::string c = "$EDITOR ~/.dotman/dotfiles";
    system(c.c_str());
    return true;
}