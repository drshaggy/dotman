//
// Created by connor on 10/11/2019.
//

#ifndef DOTMAN_COMMAND_H
#define DOTMAN_COMMAND_H

#include <iostream>
#include <vector>
#include <fstream>
#include <sstream>
#include "DotFile.h"
#include "Constants.h"

enum class Commands
{
    help,
    ls,
    edit,
    config
};

class Command {
protected:
    std::string name;
public:
    virtual bool executeCommand(std::vector<std::string>args , std::vector<DotFile>* dot_files);
    static Command* Create(Commands command);
    std::string getName(){ return this->name; }
};

class Help: public Command {
private:
public:
    Help(){ name = "help"; }
    bool executeCommand(std::vector<std::string> args, std::vector<DotFile>* dot_files);
};

class Edit: public Command {
private:
public:
    Edit() { name = "edit"; }
    bool executeCommand(std::vector<std::string> args, std::vector<DotFile>* dot_files);
};

class Ls: public Command {
private:
public:
    Ls() { name = "ls"; }
    bool executeCommand(std::vector<std::string> args, std::vector<DotFile>* dot_files);
};

class Config: public Command {
private:
public:
    Config() { name = "config"; }
    bool executeCommand(std::vector<std::string> args, std::vector<DotFile>* dot_files);
};


#endif //DOTMAN_COMMAND_H
