//
// Created by connor on 16/11/2019.
//

#ifndef DOTMAN_CONSTANTS_H
#define DOTMAN_CONSTANTS_H

#include <string>

const std::string home_dir = getenv("HOME");

const std::string DOT_FILES_PATH = home_dir + "/.dotman/dotfiles";

const std::string HELP_FILE_PATH = home_dir + "/.dotman/help";

#endif //DOTMAN_CONSTANTS_H
