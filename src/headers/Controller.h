//
// Created by connor on 16/11/2019.
//

#ifndef DOTMAN_CONTROLLER_H
#define DOTMAN_CONTROLLER_H

#include "DotFile.h"
#include "Command.h"
#include <vector>
#include <map>
#include "Constants.h"


class Controller {
private:
    std::vector<DotFile> dot_files;
    std::map<std::string, Commands> commands;
    bool commandSuccess = false;
    void registerCommands();
    bool registerDotFiles();
public:
    Controller();
    bool getCommandSuccess() { return this->commandSuccess; }
    bool runCommand(std::vector<std::string> args);
    std::vector<DotFile> getDotFiles();

};




#endif //DOTMAN_CONTROLLER_H
