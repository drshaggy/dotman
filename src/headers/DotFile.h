//
// Created by connor on 16/11/2019.
//

#ifndef DOTMAN_DOTFILE_H
#define DOTMAN_DOTFILE_H

#include <string>


class DotFile {
private:
    std::string name;
    std::string file_path;
public:
    DotFile(std::string name, std::string file_path);
    std::string getName(){ return this->name; }
    std::string getFilePath(){ return this->file_path; }

};


#endif //DOTMAN_DOTFILE_H
