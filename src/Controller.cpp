//
// Created by connor on 16/11/2019.
//

#include "headers/Controller.h"

using namespace std;

Controller::Controller()
{
    registerDotFiles();
    registerCommands();
}

void Controller::registerCommands()
{
    commands["help"] = Commands::help;
    commands["ls"] = Commands::ls;
    commands["edit"] = Commands::edit;
    commands["config"] = Commands::config;
}

bool Controller::registerDotFiles()
{

    std::ifstream file(DOT_FILES_PATH);
    if (file.is_open()) {
        std::string line;
        while (getline(file, line)) {
            std::istringstream sline(line);
            std::string word;
            std::vector<std::string> vec;
            while (getline(sline, word, ' ')) {
                vec.push_back(word);
            }
            DotFile dot_file(vec[0],vec[1]);
            this->dot_files.push_back(dot_file);
        }
        file.close();
        return true;
    }
    else
        return false;
}

bool Controller::runCommand(vector<string> args)
{
    bool isEmpty = args.size() == 0;
    Command *command;
    if(not isEmpty)
    {
        const string command_word = args[0];
        command = Command::Create(commands[command_word]);
    }
    else
    {
        args.push_back("help");
        command = Command::Create(Commands::help);
    }
    this->commandSuccess = command->executeCommand(args, &this->dot_files);
    return this->commandSuccess;
}

vector<DotFile> Controller::getDotFiles()
{
    return this->dot_files;
}