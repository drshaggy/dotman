#include "headers/headers.h"

using namespace std;


int main(int argc, char* argv[]) {
    vector<string> args(argv + 1, argv + argc);
    Controller ctl;
    ctl.runCommand(args);

    return 0;
}