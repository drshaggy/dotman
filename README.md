**Dotman**

This program gives you quick access to edit all your important dot files.


**Install**

`git clone https://gitlab.com/drshaggy/dotman.git`

`cd dotman`

`make install`

`dotman help`

**Uninstall**

`make uninstall`

**Install on Termux**

*NOTE: you may have to set your Editor environment variable first*

`git clone https://gitlab.com/drshaggy/dotman.git`

`cd dotman`

`make`

`make install-termux`

`dotman help`

**Uninstall on Termux**

`make uninstall-termux`
