TARGET_EXEC ?= dotman

BUILD_DIR ?= ./build
SRC_DIRS ?= ./src
HEAD_DIRS ?= ./src/headers

SRCS := $(shell find $(SRC_DIRS) -name *.cpp -or -name *.c -or -name *.s)
OBJS := $(SRCS:%=$(BUILD_DIR)/%.o)
DEPS := $(OBJS:.o=.d)


INC_DIRS := $(shell find $(SRC_DIRS) -type d)
INC_FLAGS := $(addprefix -I,$(INC_DIRS))

CPPFLAGS ?= $(INC_FLAGS) -MMD -MP

$(BUILD_DIR)/$(TARGET_EXEC): $(OBJS)
	$(CXX) $(OBJS) -o $@ $(LDFLAGS)

# assembly
$(BUILD_DIR)/%.s.o: %.s
	$(MKDIR_P) $(dir $@)
	$(AS) $(ASFLAGS) -c $< -o $@


# c++ source
$(BUILD_DIR)/%.cpp.o: %.cpp
	$(MKDIR_P) $(dir $@)
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) -c $< -o $@


.PHONY: clean

clean:
	$(RM) -r $(BUILD_DIR)

.PHONY: install

install: $(BUILD_DIR)/$(TARGET_EXEC)
	cp $(BUILD_DIR)/$(TARGET_EXEC) /usr/local/bin/$(TARGET_EXEC)
	mkdir -p ~/.$(TARGET_EXEC)
	cp config/help ~/.$(TARGET_EXEC)
	cp config/dotfiles ~/.$(TARGET_EXEC);


.PHONY: uninstall

uninstall:
	rm -f /usr/local/bin/$(TARGET_EXEC)
	rm -rf ~/.$(TARGET_EXEC)

install-termux:
	cp $(BUILD_DIR)/$(TARGET_EXEC) ~/../usr/bin/$(TARGET_EXEC)
	mkdir -p ~/.$(TARGET_EXEC)
	cp config/help ~/.$(TARGET_EXEC)
	cp config/dotfiles ~/.$(TARGET_EXEC);

uninstall-termux:
	rm -f ~/../usr/bin/$(TARGET_EXEC)
	rm -rf ~/.$(TARGET_EXEC)

-include $(DEPS)

MKDIR_P ?= mkdir -p
