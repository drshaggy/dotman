//
// Created by connor on 12/11/2019.
//

#include "gtest/gtest.h"
#include "../../src/headers/Command.h"

using namespace std;


TEST(TestCommand, testHelpConstruction)
{
    Command *command = Command::Create(Commands::help);
    EXPECT_EQ("help", command->getName());
}

TEST(TestCommand, testLsConstruction)
{
    Command *command = Command::Create(Commands::ls);
    EXPECT_EQ("ls", command->getName());
}

TEST(TestCommand, testEditConstuction)
{
    Command *command = Command::Create(Commands::edit);
    EXPECT_EQ("edit", command->getName());
}

TEST(TestCommand, testHelpCommandFileOpens)
{
    std::vector<std::string> args;
    Command *command = Command::Create(Commands::help);
    vector<DotFile> dot_files;
    EXPECT_TRUE(command->executeCommand(args, &dot_files));
}

