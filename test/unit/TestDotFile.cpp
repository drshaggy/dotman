//
// Created by connor on 16/11/2019.
//

#include "gtest/gtest.h"
#include "../../src/headers/DotFile.h"


TEST(TestDotFile, testConstruction)
{
    bool itWorks = false;
    try
    {
        DotFile dot_file("ssh", "/home/connor/.ssh/config");
        itWorks = true;
    }
    catch (...)
    {
        itWorks = false;
    }
    EXPECT_TRUE(itWorks);
}
