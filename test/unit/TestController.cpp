//
// Created by connor on 16/11/2019.
//

#include "gtest/gtest.h"
#include "../../src/headers/Controller.h"

using namespace std;

TEST(TestController, testHelpCommand)
{
    vector<string> args;
    args.push_back("help");
    Controller ctl;
    bool itWorks = ctl.runCommand(args);
    EXPECT_TRUE(itWorks);
}

TEST(TestController, testParsingDotFile)
{
    Controller ctl;
    vector<string> answer_names;
    answer_names.push_back("bash");
    answer_names.push_back("ssh");
    answer_names.push_back("zsh");

    vector<string> answer_paths;
    answer_paths.push_back("~/.bashrc");
    answer_paths.push_back("~/.ssh/config");
    answer_paths.push_back("~/.zshrc");



    vector<DotFile> dot_files = ctl.getDotFiles();
    int i = 0;
    for (auto &dot_file: dot_files)
    {
        EXPECT_EQ(dot_file.getName(), answer_names[i]);
        EXPECT_EQ(dot_file.getFilePath(),answer_paths[i]);
        i++;
    }
}

TEST(TestController, testLsCommand)
{
    vector<string> args;
    args.push_back("ls");
    Controller ctl;
    bool itWorks = ctl.runCommand(args);
    EXPECT_TRUE(itWorks);
}